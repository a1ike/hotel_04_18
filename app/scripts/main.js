$(document).ready(function () {

  $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.h-header__menu').on('click', function (e) {
    e.preventDefault();
    $('.h-header__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.h-bottom__menu').on('click', function (e) {
    e.preventDefault();
    $('.h-bottom__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.h-catalog__card-button').on('click', function (e) {
    e.preventDefault();
    $('.h-popup__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.h-popup__close').on('click', function (e) {
    e.preventDefault();
    $('.h-popup__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.h-tariffs__card-button').on('click', function (e) {
    e.preventDefault();
    $('.h-popup__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  wow = new WOW(
    {
      boxClass: 'wow',      // default
      animateClass: 'animated', // default
      offset: 0,          // default
      mobile: false,       // default
      live: true        // default
    }
  )
  wow.init();

  /* $('#form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var goods = $('input[name="goods"]', f).val();
    var types = $('input[name="types"]', f).val();
    var sizes = $('input[name="sizes"]', f).val();
    var amount = $('input[name="amount"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        $('.c-collect__flex').slideToggle('fast', function (e) {
          // callback
        });
        $('.c-stepper__wrapper').slideToggle('fast', function (e) {
          // callback
        });
        window.location.replace('order.php');
      }
      else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  }); */

});

